"CmdLine script to configure the initial minimal image"

| logger |

logger := FileStream stderr.
logger cr; nextPutAll: 'Starting Minimal Config Script...'.

Metacello new
    baseline: 'LambdaPrerequisites';
    repository: 'filetree://../bootstrap';
    load.


"Add a Stub Pharo7 Platform to compensate for using Pharo7 minimal image"
Smalltalk at: #FLPharo7Platform ifAbsent: [
    logger cr; nextPutAll: '>Creating FLPharo7Platform'.
    (Smalltalk at: #FLPharo6Platform) subclass: #FLPharo7Platform.
    (Smalltalk at: #FLPharo6Platform) class compile: 'isResponsibleForCurrentPlatform
            ^true'.
].

"Check status of 64bit image hashing deficiencies"
(SmallFloat64 class selectors includes: #materializeFrom:) ifTrue: [
    logger cr; nextPutAll: 'WARNING: SmallFloat64 class>>#materializeFrom has been fixed.'.
].

(SmallFloat64 selectors includes: #basicIdentityHash:) ifTrue: [
    logger cr; nextPutAll: 'WARNING: SmallFloat64>>#basicIdentityHash has been fixed.'.
].

logger cr; nextPutAll: 'Finished Script.'; cr; cr.