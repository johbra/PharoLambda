"Configure the environment"

| configParams accessKey secretKey regionKey bucketKey |

"Expect image to be called with params as a last arg array"
configParams := Array readFrom: Smalltalk arguments last.
accessKey := configParams at: 1.
secretKey := configParams at: 2.
regionKey := configParams at: 3.
bucketKey := configParams at: 4.

AWSS3Config default
    accessKeyId: accessKey;
	secretKey: secretKey;
	regionName: regionKey;
	at: #defaultBucket put: bucketKey;
	yourself.

"Stop logging changes and reading source files for read/only environment"
(Smalltalk at: #NoChangesLog) install.
(Smalltalk at: #NoPharoFilesOpener) install.