*LambdaRuntime
writeFuelContext: aContext

			| s3 bucket stderr serializer aStream |
			
			stderr := FileStream  stderr
					wantsLineEndConversion: true;
					converter;
					yourself.
					
			stderr nextPutAll: 'Writing fuel context to S3...'; cr.
			
			s3 := AWSS3Zip new.
			bucket := s3 bucketNamed: (s3 awsConfig at: #defaultBucket).
			
			stderr nextPutAll: 'Using S3 bucket ', bucket bucketName; cr.
			
			serializer := FLSerializer newDefault.
    		serializer analyzer 
        		when: [ :o | o class isObsolete ] 
        		substituteBy: [ :o | nil ].
		
    		aStream := GZipWriteStream on: (ByteArray new: 100).
	  		serializer serialize: aContext on: aStream.
			aStream close.
			
			stderr nextPutAll: 'Serialized context'; cr.
			
			bucket
				atKey: 'PharoDebug.fuel.zip'
				putObject: aStream encodedStream contents.
				
			stderr nextPutAll: 'S3 store complete!'; cr. 