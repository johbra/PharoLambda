private
getBucket: bucketNameString
	^ (super getBucket: bucketNameString)
		service: self;
		yourself