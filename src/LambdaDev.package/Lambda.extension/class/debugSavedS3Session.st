*LambdaDev
debugSavedS3Session
	"Open a debugger on a saved lambda session.
	NOTE: Assumes that AWSS3Config has been initialized"
		
	self debugS3SessionNamed: 'PharoDebug.fuel.zip'